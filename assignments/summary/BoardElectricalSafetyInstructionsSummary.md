# Board Electrical Saftey Instructions

## What is electrical safety?

Electrical safety is a system of organizational measures and technical means to prevent harmful and dangerous effects on workers from electric current, electric arc, electromagnetic field and static electricity.

## Do's and Don'ts

### 1.Power Supply:

![pS](https://image.slidesharecdn.com/hazardsandrisks-161118031400/95/hazards-and-risks-12-638.jpg?cb=1479438913)

**Do's**

* Always make sure that the output voltage of the power supply matches the input voltage of the board.
* While turning on the power supply make sure every circuit is connected properly.

**Don'ts**

* Do not connect power supply without matching the power rating.
* Never connect a higher output(12V/3Amp) to a lower (5V/2Amp) input .
* Do not try to force the connector into the power socket ,it may damage the connector.

### 2.Handling:

![handling](https://www.fixwerks.com/wp-content/uploads/2016/11/IMG_1074-600x200.jpg)

**Do's**

* Treat every device like it is energized, even if it does not look like it is plugged in or operational.
* While working keep the board on a flat stable surface (wooden table).
* Unplug the device before performing any operation on them.
* When handling electrical equipment, make sure your hands are dry.
* Keep all electrical circuit contact points enclosed.
* If the board becomes too hot try to cool it with a external usb fan.

**Don'ts**

* Don’t handle the board when its powered ON.
* Never touch electrical equipment when any part of your body is wet, (that includes fair amounts of perspiration).
* Do not touch any sort of metal to the development board.

### 3.GPIO 

![GPIO](https://projects-static.raspberrypi.org/projects/physical-computing-with-scratch/702273e5f1211f7041b6d1dc3939944cf0b99409/en/images/led-3v3.png)

**Do's**

* Find out whether the board runs on 3.3v or 5v logic.
* Always connect the LED (or sensors) using appropriate resistors.
* To Use 5V peripherals with 3.3V we require a logic level converter.

**Don’ts**

* Never connect anything greater that 5v to a 3.3v pin.
* Avoid making connections when the board is running.
* Don't plug anything with a high (or negative) voltage.
* Do not connect a motor directly , use a transistor to drive it.

## Guidelines for using interfaces

### UART : Universal Asychronous Receiver Transmitter

![UART_interface](https://www.codrey.com/wp-content/uploads/2017/10/UART-Interface.png)

* Connect Rx pin of device1 to Tx pin of device2 ,similarly Tx pin of device1 to Rx pin of device2.
* If the device1 works on 5v and device2 works at 3.3v then use the level shifting mechanism(voltage divider )
* Genrally UART is used to communicate with board through USB to TTL connection . USB to TTL connection does not require a protection circuit .
* Whereas Senor interfacing using UART might require a protection circuit.

### I2C : Inner Integrated Circuit

![I2C](https://silvaco.com.cn/products/IP/i2c-interface/I2C_diagram.gif)

* while using I2C interfaces with sensors SDA and SDL lines must be protected.
* Protection of these lines is done by using pullup registers on both lines.
* If you use the inbuilt pullup registers in the board you wont need an external circuit.
* if you are using bread-board to connect your sensor , use the pullup resistor .
* Generally , 2.2kohm <= 4K ohm resistors are used.

### SPI : Serial Peripheral Interface

![SPI](https://lh3.googleusercontent.com/proxy/-nt6k_5ibcRWBDpfMmqMP3h4AnxZIU1LxlNo_H0pZzdag9KCcMovNWSLXOZ-vwS0gHlVPm5GxC33eTkrMOIGRSky9fBhdDACg9K2alsQPVLU68NoFF3DLB6IrdUVgEMxR__dAmIsKEelPg-s2G0MlzQSgL0wJNHsO64e20U0QFPVdroQuT6I96HpOZAK9AIHqMJs7Qjc2oQudJl7BkmU6--0Al0N7QfHP37uHrwUDiBznA9E0NXUTUZxWyvh35w5ObaMl2_gvpZJIbYJ5Q)

* Generally ,SPI in development boards is in Push-pull mode.
* Push-pull mode does not require any protection circuit.
* On SPI interface if you are using more than one slaves it is possible that the device2 can "hear" and "respond" to the master's communication with device1- which is an disturbance .
* To overcome this problem , we use a protection circuit with pullup resistors on each the Slave Select line(CS).
* Resistors value can be between 1kOhm ~10kOhm . Generally 4.7kOhm resistor is used.

