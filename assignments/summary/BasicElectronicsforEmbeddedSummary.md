# BASIC ELECTRONICS FOR EMBEDDED

## 1.Sensors and Actuators

![Sensors and Actuators](https://miro.medium.com/max/1646/0*NIzIJMOgy6Aich2m.)

### Sensors:

 ![sensors](https://5.imimg.com/data5/XP/RS/MY-9380557/sensors-500x500.jpg)

  A sensor monitors environmental conditions such as fluid levels, temperatures, vibrations, or voltage. When these environmental conditions change, they send an electrical signal to the sensor, which can then send the data or an alert back to a centralized computer system or adjust the functioning of a particular piece of equipment. For example, if a motor reaches the temperature point of overheating, it can automatically shut off.<br/>
  These are also called as sensary organs(inputs) for electronic devices, just like humans have eyes, ears and nose. Electronics devices also have camera, microphone and smoke detectors.


### Actuators:

![actuators](https://www.elprocus.com/wp-content/uploads/actuators.jpg)

 An actuator, on the other hand, causes movement. It takes an electrical signal and combines it with an energy source to create physical motion. An actuator may be pneumatic, hydraulic, electric, thermal, or magnetic. For example, an electrical pulse may drive the functioning of a motor within an asset.<br/>
 In simple words actuators work in reverse fashion of the sensors, that is actuators are mainly focused for producing outputs.

## 2.Analog and Digital

![Analog and Digital](https://www.electronicshub.org/wp-content/uploads/2019/03/Analog-Circuits-and-Digital-Circuits-Featured-Image.jpg)

### Analog:
 
 The term analog represents continuous and everything in nature is the analog pattern only.

### Digital:
 
 The term digital represents discrete and two values of data, that is 1 and 0 which is know as Binary language of electronic devices.

### Analog vs Digital:

![AnaVsDig](assignments/pics/AnaVsDig.png)

## 3.Micro-controller Vs Micro-processor

![ConVsPro](https://www.microcontrollertips.com/wp-content/uploads/2017/10/Fig-1-MicrocontrollerMicroprocesser-opener.jpg)

You must always be confused when you are asked about difference between microprocessors and microcontrollers. As it seems to be same but it’s not.

### Micro-controller:
 
 ![mic-controller](https://akhiljain07.files.wordpress.com/2012/10/untitled1.png)

 It’s like a small computer on a single IC. It contains a processor core, ROM, RAM and I/O pins dedicated to perform various tasks. Microcontrollers are generally used in projects and applications that require direct control of user. As it has all the components needed in its single chip, it does not need any external circuits to do its task so microcontrollers are heavily used in embedded systems and major microcontroller manufacturing companies are making them to be used in embedded market. A microcontroller can be called the heart of embedded system. Some examples of popular microcontrollers are 8051, AVR, PIC series of microcontrollers.

### Micro-processor:

 ![mic-processor](https://akhiljain07.files.wordpress.com/2012/10/untitled.png)

 Microprocessor has only a CPU inside them in one or few Integrated Circuits. Like microcontrollers it does not have RAM, ROM and other peripherals. They are dependent on external circuits of peripherals to work. But microprocessors are not made for specific task but they are required where tasks are complex and tricky like development of software’s, games and other applications that require high memory and where input and output are not defined. It may be called heart of a computer system.  Some examples of microprocessor are Pentium, I3, and I5 etc.

 ## 4.Introduction to Rasberry PI 

 The Raspberry Pi is a small computer that can do lots of things. You plug it into a monitor and attach a keyboard and mouse.

 ![Rasberry PI](https://www.blogdot.tv/wp-content/uploads/2018/08/getting-started-with-your-raspberry-pi.gif)

 ### what we need?

  #### Hardware:

   * A Raspberry Pi computer with an SD card or micro SD card

   * A monitor with a cable (and, if needed, an HDMI adaptor)

   * A USB keyboard and mouse

   * A power supply

   * Headphones or speakers (optional)

   * An ethernet cable (optional)

   #### software:

   * Raspbian, installed using the Raspberry Pi Imager

  ### what are Interfaces?

  ![Interfaces](https://cdn.shopify.com/s/files/1/0176/3274/articles/rpi_600x600_crop_center.png?v=1558435064)

* USB ports — these are used to connect a mouse and keyboard. You can also connect other components, such as a USB drive.

* SD card slot — you can slot the SD card in here. This is where the operating system software and your files are stored.

* Ethernet port — this is used to connect Raspberry Pi to a network with a cable. Raspberry Pi can also connect to a network via wireless LAN.

* Audio jack — you can connect headphones or speakers here.

* HDMI port — this is where you connect the monitor (or projector) that you are using to display the output from the Raspberry Pi. If your monitor has speakers, you can also use them to hear sound.

* Micro USB power connector — this is where you connect a power supply. You should always do this last, after you have connected all your other components.

* GPIO ports — these allow you to connect electronic components such as LEDs and buttons to Raspberry Pi.

## 5.Serial and Parallel 

### Serial and Parallel communication:

![PvsS](https://techdifferences.com/wp-content/uploads/2016/04/Untitled.jpg)

The crucial difference between serial and parallel communication is that in serial communication a single communication link is used to transfer the data from an end to another. As against in parallel communication, multiple parallel links are used that transmits each bit of data simultaneously.

### Serial interface:

1.**UART** - Universal asynchronous receiver-transmitter (UART) is one of the simplest and oldest forms of device-to-device digital communication. You can find UART devices as a part of integrated circuits (ICs) or as individual components. UARTs communicate between two separate nodes using a pair of wires and a common ground.

2.**SPI** - The Serial Peripheral Interface (SPI) is a synchronous serial communication interface specification used for short-distance communication, primarily in embedded systems. The interface was developed by Motorola in the mid-1980s and has become a de facto standard.

3.**I2C** - Inter-Integrated Circuit, pronounced I-squared-C, is a synchronous, multi-master, multi-slave, packet switched, single-ended, serial communication bus invented in 1982 by Philips Semiconductor.

### Parallel interface:

1.**GPIO** - GPIO stands for General Purpose Input/Output. It's a standard interface used to connect microcontrollers to other electronic devices. For example, it can be used with sensors, diodes, displays, and System-on-Chip modules.

